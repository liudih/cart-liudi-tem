package com.tomtop.controllers;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tomtop.entity.order.PaymentError;
import com.tomtop.exceptions.BadRequestException;
import com.tomtop.exceptions.CreateOrderException;
import com.tomtop.exceptions.DiscountException;
import com.tomtop.exceptions.InventoryShortageException;
import com.tomtop.exceptions.MemberAddressException;
import com.tomtop.exceptions.OrderNocompleteException;
import com.tomtop.utils.Request;

@Controller
@RequestMapping("/exception")
public class ExceptionController {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ExceptionController.class);

	@RequestMapping("/discount")
	public String discount(HttpServletRequest request, Map<String, Object> model) {
		DiscountException e = (DiscountException) request
				.getAttribute("javax.servlet.error.exception");
//		LOGGER.error("discount error", e);
		
		return this.errorPage(e.getCode(), e.getMessage());
		
	}

	@RequestMapping("/bad-request")
	public String badRequest(HttpServletRequest request,
			Map<String, Object> model) {
		BadRequestException e = (BadRequestException) request
				.getAttribute("javax.servlet.error.exception");

//		LOGGER.error("bad-request error", e);

		return this.errorPage("BadRequest", e.getMessage());
	}

	@RequestMapping("/create-order")
	public String createOrder(HttpServletRequest request,
			Map<String, Object> model) {
		CreateOrderException e = (CreateOrderException) request
				.getAttribute("javax.servlet.error.exception");

//		LOGGER.error("create-order error", e);

		return this.errorPage("create order error", e.getMessage());
	}

	@RequestMapping("/invalid-storage")
	public String invalidStorage(HttpServletRequest request,
			Map<String, Object> model) {

		Exception e = (Exception) request
				.getAttribute("javax.servlet.error.exception");

//		LOGGER.error("invalid-storage", e);
		return this.errorPage("invalid storage", e.getMessage());
	}

	@RequestMapping("/member-address")
	public String memberAddress(HttpServletRequest request,
			Map<String, Object> model) {

		MemberAddressException e = (MemberAddressException) request
				.getAttribute("javax.servlet.error.exception");

//		LOGGER.error("member-address", e);

		return this.errorPage("invalid address", e.getMessage());
	}

	@RequestMapping("/illegal-argument")
	public String illegalArgument(HttpServletRequest request,
			Map<String, Object> model) {

		IllegalArgumentException e = (IllegalArgumentException) request
				.getAttribute("javax.servlet.error.exception");

//		LOGGER.error("illegal-argument", e);

		// 跳到失败页面
		return this.errorPage("", e.getMessage());
	}

	@RequestMapping("/order-no-complete")
	public String orderNoComplete(HttpServletRequest request,
			Map<String, Object> model) {

		OrderNocompleteException e = (OrderNocompleteException) request
				.getAttribute("javax.servlet.error.exception");

//		LOGGER.error("order-no-complete", e);

		return this.errorPage("", e.getMessage());
	}

	@RequestMapping("/user-no-login")
	public String userNoLongin(HttpServletRequest request,
			Map<String, Object> model) {

		Exception e = (Exception) request
				.getAttribute("javax.servlet.error.exception");

//		LOGGER.error("user-no-login", e);

		return this.errorPage("noLogin", "you must first login");
	}

	@RequestMapping("/null-pointer")
	public String exception(HttpServletRequest request,
			Map<String, Object> model) {

		Exception e = (Exception) request
				.getAttribute("javax.servlet.error.exception");

//		LOGGER.error("null-pointer", e);

		return this.errorPage("null pointer", "so sorry,error occured");
	}

	@RequestMapping("/notfound")
	public String notfound(HttpServletRequest request, Map<String, Object> model)
			throws IOException {
		HttpServletResponse Response = Request.currentResponse();
		Response.sendRedirect("/");
		return null;
	}

	@RequestMapping("/server-error")
	public String serverError(HttpServletRequest request,
			Map<String, Object> model) throws IOException {

		Exception e = (Exception) request
				.getAttribute("javax.servlet.error.exception");

//		LOGGER.error("server-error", e);

		return this.errorPage("500", "so sorry,error occured.");
	}
	
	
	public String errorPage(String code, String message){
		code = code==null ? "" : code;
		// 跳转到支付失败页面
		StringBuilder errorurl = new StringBuilder("/payment-result/error");
		errorurl.append("?errorCode=").append(code);
		errorurl.append("&error=").append(message);
		try {
			Request.currentResponse().sendRedirect(errorurl.toString());
			return null;
		} catch (Exception e) {
			return null;
		}
	}
	
	@RequestMapping("/inventory-shortage")
	public String inventoryShortage(HttpServletRequest request,Map<String, Object> model) {
		InventoryShortageException e = (InventoryShortageException) request
				.getAttribute("javax.servlet.error.exception");

		// 跳到失败页面
		PaymentError error = new PaymentError("inventory shortage", null,
				e.getMessage());
		model.put("error", error);
		return "order/pay_fail";
	}

}
