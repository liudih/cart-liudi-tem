package com.tomtop.controllers.cart;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tomtop.utils.CookieUtils;

@Controller
@RequestMapping("/")
public class BaseController {
	private static final Logger logger = LoggerFactory.getLogger(BaseController.class);
	
	@RequestMapping(value = "/setstorageid", method = RequestMethod.GET)
	@ResponseBody
	public Object showHotKey(Model model, 
		@RequestParam(value = "storageid", required = false, defaultValue = "1") String storageid) {
		Map<String, Object> mjson = new HashMap<String, Object>();
		CookieUtils.setCookie("storageid", storageid);
		mjson.put("result", "success");
		
		return mjson;
	}
	
	
}
