package com.tomtop.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.chain.web.WebContext;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.util.Lists;
import com.tomtop.bo.CountryBo;
import com.tomtop.bo.CurrencyBo;
import com.tomtop.configuration.CartSettings;
import com.tomtop.dto.Country;
import com.tomtop.dto.ISOCountry;
import com.tomtop.dto.base.Website;
import com.tomtop.services.base.IWebsiteService;
import com.tomtop.valueobjects.Constants;
import com.tomtop.valueobjects.base.LoginContext;
import com.tomtop.valueobjects.product.ProductBasePriceReviewInfoVo;

@Service
public class FoundationService {

	public static final String COOKIE_LANG = "TT_LANG";
	// public static final String PLAY_LANG = "PLAY_LANG";
	public static final String COOKIE_DEVICE = "TT_DEVICE";
	public static final String COOKIE_CURRENCY = "TT_CURR";
	public static final String COOKIE_COUNTRY = "TT_COUN";
	public static final String COOKIE_SHIPTO = "TT_SHIPTO";

	public static final String TT_LTC = "TT_LTC";
	public static final String TT_TOKEN = "TT_TOKEN";
	public static final String TT_UUID = "TT_UUID";
	public static final String STORAGE_ID = "storageid";
	public static final String WEB_HISTORY = "WEB-history";

	public static final String DOMAIN = ".chicuu.com";
	public static final String HOST = "chicuu.com";

	public static final String BASE_URL = "http://base.api.tomtop.com";
	public static final String MEMBER_URL = "http://member.api.tomtop.com";
	public static final String PRODUCT_URL = "http://product.api.tomtop.com";
	public static final String LOYALTY_URL = "http://loyalty.api.chicuu.com";

//	public static final Pattern PATTERN = Pattern
//			.compile("\\w+\\.([\\w]+)\\..*");
	public static final Pattern PATTERN = Pattern
			.compile("(\\w+\\.)*([\\w]+)\\.");
	
	// 提取host倒数第二个"."和倒数第一个"."中间的字符串,移动端以"m."开头
	// 如:移动="m.cart.chicuu.com:9005/",pc="cart.chicuu.com:9005/"
	public static final Pattern PATTERN_MOBILE = Pattern
			.compile("(\\w+\\.)*([\\w]+)\\.");

	private List<ISOCountry> isocountryList;

	Logger logger = LoggerFactory.getLogger(FoundationService.class);

	@Autowired
	HttpRequestFactory requestFactory;

	@Autowired
	IWebsiteService wsService;

	@Value("${getLoginContextUrl}")
	String getLoginContextUrl;

	@Value("${payment.logo}")
	String logo;

	@Value("${chicuu.ico}")
	String chicuuIco;

	@Value("${tomtop.ico}")
	String tomtopIco;

	@Autowired
	CartSettings cartSettings;

	/**
	 * 获取当前语言
	 * 
	 * @return
	 */
	public int getLang() {
		String cookie = CookieUtils.getCookie(COOKIE_LANG);
		if (StringUtils.isEmpty(cookie)) {
			return 1;
		}
		String slang = cookie;
		if (StringUtils.isNumeric(slang) && !"".equals(slang)) {
			return Integer.parseInt(slang);
		} else {
			return 1;
		}
	}

	/**
	 * 获取当前站点id
	 * 
	 * @return
	 */
	public int getSiteID() {
		Website site = wsService.getWebsite(this.getVhost());
		if (site != null) {
			return site.getIid();
		}
		return 1;
	}
	
	/**
	 * 根据终端类型获取当前站点id
	 * @param terminalType 访问终端类型,pc端或移动端
	 * @return
	 */
	public int getSiteID(String terminalType) {
		Website site = null;
		if(Constants.TERMINAL_TYPE_PC.equals(terminalType)){
			site = wsService.getWebsite(this.getVhost());
		}else{
			site = wsService.getWebsite(this.getMobileVhost());
		}
		if (site != null) {
			return site.getIid();
		}
		return 1;
	}

	/**
	 * 获取当前客户端id
	 * 
	 * @return
	 */
	public int getClientID() {
		return 10;
	}

	/**
	 * 获取当前货币字符串
	 * 
	 * @return
	 */
	public String getCurrency() {
		String cookie = CookieUtils.getCookie(COOKIE_CURRENCY);
		if (StringUtils.isEmpty(cookie)) {
			return "USD";
		} else {
			return cookie;
		}
	}

	/**
	 * 获取当前货币对象
	 * 
	 * @return
	 */
	public CurrencyBo getCurrencyBo() {
		String curr = getCurrency();
		String cur = HttpClientUtil.doGet(BASE_URL
				+ "/base/currency/v1/where?code=" + curr);
		if (cur == null || "".equals(cur)) {
			return null;
		}
		JSONObject object = JSON.parseObject(cur);
		String arr = object.getJSONArray("data").getJSONObject(0)
				.toJSONString();
		CurrencyBo currbo = JSON.parseObject(arr, CurrencyBo.class);
		return currbo;
	}

	/**
	 * 获取当前用户邮箱
	 * 
	 * @return
	 */
	// public String getLoginEmail(HttpServletRequest request) {
	// String co = CookieUtils.getCookie(TT_UUID);
	// if (StringUtils.isEmpty(co)) {
	// return "";
	// }
	// String uuid = co;
	// String cur = HttpClientUtil.doGet(MEMBER_URL + "/member/v1/email/"
	// + uuid);
	// if (cur == null || "".equals(cur)) {
	// return "";
	// }
	// JSONObject object = JSON.parseObject(cur);
	// Integer ret = object.getInteger("ret");
	// if (ret != null && ret == 1) {
	// String email = object.getJSONObject("data").getString("email");
	// logger.debug("email===" + email);
	// return email;
	// } else {
	// return "";
	// }
	// }

	/**
	 * 获取所有国家信息，（国家下拉框作用）
	 * 
	 * @return
	 */
	public List<ISOCountry> getAllCountries() {
		if (this.isocountryList != null && this.isocountryList.size() > 0) {
			return this.isocountryList;
		} else {
			String cur = HttpClientUtil.doGet(BASE_URL + "/base/country/v1");
			if (cur == null || "".equals(cur)) {
				return Lists.newArrayList();
			}
			JSONObject object = JSON.parseObject(cur);
			JSONArray arr = object.getJSONArray("data");
			List<ISOCountry> clist = Lists.newArrayList();
			arr.forEach(c -> {
				CountryBo co = JSON.parseObject(c.toString(), CountryBo.class);
				clist.add(new ISOCountry(co.getId(), co.getName(), co
						.getIsoCodeTwo()));
			});
			this.isocountryList = clist;
			return this.isocountryList;
		}
	}

	/**
	 * 获取当前国家对象
	 * 
	 */
	public Country getCountryObj() {
		Country country = new Country();
		String co = CookieUtils.getCookie(COOKIE_SHIPTO);
		if (StringUtils.isEmpty(co)) {
			country.setCname("United States");
			country.setCshortname("US");
			return country;
		}
		String c = co;
		try {
			c = URLDecoder.decode(c, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error(e.toString());
		}
		String[] arr = c.split("|");
		if (arr.length > 0) {
			country.setCname(arr[0]);
			country.setCshortname(arr[1]);
		} else {
			country.setCname("United States");
			country.setCshortname("US");
		}
		return country;
	}

	/**
	 * 获取浏览历史记录
	 * 
	 * @return
	 */
	public List<ProductBasePriceReviewInfoVo> getHistoryProducts() {
		String co = CookieUtils.getCookie("WEB-history");
		if (StringUtils.isNotEmpty(co)) {
			String history = co;
			logger.debug("history==" + history);
			// String history =
			// "9d326eb7-fd30-426f-b26e-b7fd30826f6d,00aa3424-d914-1004-874c-d371c9ab96c0,00b8ccd9-d914-1004-874c-d371c9ab96c0,d4c3dcd8-d929-1004-835b-90389054983d,04d82b3b-ae0b-41cb-982b-3bae0b71cbe0,2cf4bee2-4a51-43a3-b4be-e24a5103a3e3,58b69c91-6c31-406c-b69c-916c31c06c35,643fb0c5-3a52-48cf-bfb0-c53a52f8cf0e,4c231e38-51be-453c-a31e-3851be153c31,";
			try {
				history = URLDecoder.decode(history, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				logger.error(e.toString());
			}
			int lang = getLang();
			int siteid = getSiteID();
			String currency = getCurrency();
			String data = HttpClientUtil.doGet(PRODUCT_URL
					+ "/ic/v1/products?listingIds=" + history + "&lang=" + lang
					+ "&client=" + siteid + "&currency=" + currency);
			if (data != null) {
				logger.debug("dd===" + data);
				JSONObject object = JSON.parseObject(data);
				JSONArray arr = object.getJSONArray("data");
				List<ProductBasePriceReviewInfoVo> list = JSON.parseArray(
						arr.toJSONString(), ProductBasePriceReviewInfoVo.class);
				return list;
			}
			return Lists.newArrayList();
		} else {
			return Lists.newArrayList();
		}
	}

	public LoginContext getLoginContext() {
		// do cache
		LoginContext ctx = Request.currentLoginCtx();

		if (ctx == null) {

			ctx = new LoginContext(null, null, null, 1, null);

			HttpServletRequest crequest = Request.currentRequest();
			try {
				// 调用member接口取登录信息

				String urlString = getLoginContextUrl;

				if (urlString == null || urlString.length() == 0) {
					throw new NullPointerException(
							"can not get config:sendEventUrl");
				}
				GenericUrl url = new GenericUrl(urlString);

				HttpRequest request = requestFactory.buildGetRequest(url);
				HttpHeaders headers = new HttpHeaders();
				String cookie = crequest.getHeader("Cookie");
				headers.setCookie(cookie);
				request.setHeaders(headers);

				String result = request.execute().parseAsString();
				JSONObject json = JSONObject.parseObject(result);
				Integer ret = json.getInteger("ret");
				if (1 == ret) {
					String data = json.getString("data");
					ctx = JSONObject.parseObject(data, LoginContext.class);
				}
			} catch (Exception e) {
				logger.error("getLoginContext from member error", e);
			}
			Request.setLoginCtx(ctx);
		}
		return ctx;
	}

	public int getLanguage(WebContext context) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getSiteID(WebContext context) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getLanguage() {
		return this.getLang();
	}

	public String getVhost() {
		String host = Request.currentRequest().getHeader(Constants.HOST);
		Matcher matcher = PATTERN.matcher(host);
		if (matcher.find()) {
			//如果是m端
			String hostarr[] = host.split("\\.");
			if(hostarr.length>0 && "m".equals(hostarr[0])){
				return "m." + matcher.group(2) + ".com";
			}
			return "www." + matcher.group(2) + ".com";
		}
		return host;
	}
	
	/**
	 * 获取移动端访问的域名(提取host倒数第二个"."和倒数第一个"."中间的字符串)
	 * @return
	 */
	public String getMobileVhost() {
		String host = Request.currentRequest().getHeader(Constants.HOST);
		Matcher matcher = PATTERN_MOBILE.matcher(host);
		if (matcher.find()) {
			return "www." + matcher.group(2) + ".com";
		}
		
		return host;
	}

	public String getClientIP() {
		String ip = Request.currentRequest().getRemoteAddr();
		return ip;
	}

	public Object getDevice() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 获取主站域名,购物车回到主站时要通过该方法来获取主站域名
	 * 
	 * @author lijun
	 * @return
	 */
	public String getMainDomain() {
		return "http://" + this.getVhost();
	}

	/**
	 * 获取静态资源前缀
	 * 
	 * @author lijun
	 */
	public String getStaticResourcePrefix() {
		String host = this.getVhost();
		Matcher matcher = PATTERN.matcher(host);
		if (matcher.find()) {
			String hostname = matcher.group(2);
			logger.debug("~~~~~~~~~domain:{}", hostname);
			if ("tomtop".equalsIgnoreCase(hostname)) {
				String[] hostarr = host.split("\\.");
				if(hostarr.length>0 && "m".equals(hostarr[0])){
					return "http://static.tomtop-cdn.com/mtomtop/cart/";
				}
				return "http://static.tomtop-cdn.com/tomtop/cart/";
			} else {
				return "http://static." + hostname + ".com/cart/";
			}
		}
		return "/";
	}

	public String getHost() {
		String host = Request.currentRequest().getHeader(Constants.HOST);
		return host;
	}

	/**
	 * 获取hostname
	 * 
	 * @return
	 */
	public String getHostName() {
		String host = this.getVhost();
		Matcher matcher = PATTERN.matcher(host);
		if (matcher.find()) {
			return matcher.group(2);
		} else {
			return "tomtop";
		}
	}
	
	/**
	 * 获取移动端访问的hostname,移动端url以"m."开头
	 * 
	 * @return
	 */
	public String getMobileHostName() {
		String host = this.getMobileVhost();
		Matcher matcher = PATTERN_MOBILE.matcher(host);
		if (matcher.find()) {
			return matcher.group(2);
		} else {
			return "tomtop";
		}
	}

	/**
	 * 获取ico图片地址
	 * 
	 * @return
	 */
	public String getIco() {
		String host = this.getHostName();
		if ("chicuu".equals(host)) {
			return chicuuIco;
		} else {
			return tomtopIco;
		}
	}

	/**
	 * 获取logo用于支付
	 * 
	 * @return
	 */
	public String getLogo() {
		String prefix = this.getStaticResourcePrefix();
		String result = null;
		if (!prefix.endsWith("/")) {
			result = prefix + "/" + logo;
		} else {
			result = prefix + logo;
		}

		return result;
	}

	public String getImgUrlPrefix() {
		String domain = this.getHostName();
		String purl = cartSettings.getPrefix(domain);
		if(purl==null){
			return cartSettings.getPrefix("tomtop");
		}else{
			return purl;
		}
	}

	/**
	 * 获取广告联盟AID
	 * 
	 * @author lijun
	 * @return
	 */
	public String getOrigin() {
		return CookieUtils.getCookie("AID");
	}
	
	/**
	 * 获取子域名
	 * 
	 * @return
	 */
	public String getSubdomains() {
		String host = this.getHost();
		if(host!=null){
			String[] hostarr = host.split("\\.");
			if(hostarr.length>0){
				return hostarr[0];
			}else{
				return "cart";
			}
		}
		return "cart";
	}
}
