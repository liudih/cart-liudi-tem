package com.tomtop.configuration;

import java.util.Locale;

import javax.servlet.Filter;
import javax.sql.DataSource;

import liquibase.integration.spring.SpringLiquibase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.tomtop.customizer.MyCustomizer;
import com.tomtop.iterceptors.ViewModelInterceptor;

@Configuration
@EnableConfigurationProperties({ JdbcConnectionSettings.class,
		PaymentSettings.class, CartSettings.class })
public class ApplicationConfigurations {

	private static final Logger Logger = LoggerFactory
			.getLogger(ApplicationConfigurations.class);

	@Autowired
	private ViewModelInterceptor interceptor;

	
	@Bean
	public SpringLiquibase liquibase(DataSource dataSource) {
		SpringLiquibase lb = new SpringLiquibase();
		lb.setDataSource(dataSource);
		lb.setChangeLog("classpath:liquibase/order.xml");
		// contexts specifies the runtime contexts to use.
		return lb;
	}

	// @Bean(name = "multipartResolver")
	// public MultipartResolver multipartResolver() {
	// StandardServletMultipartResolver multi = new
	// StandardServletMultipartResolver();
	// return multi;
	// }

	@Bean
	public LocaleResolver localeResolver() {
		CookieLocaleResolver resolver = new CookieLocaleResolver();
		resolver.setDefaultLocale(new Locale("en"));
		resolver.setCookieName("locale");
		resolver.setCookieMaxAge(60 * 60 * 24 * 365);
		return resolver;
	}

	@Bean
	public Filter shallowEtagHeaderFilter() {
		return new ApplicationContextHeaderFilter();
	}

	@Bean
	public HttpRequestFactory httpRequestFactory() {
		NetHttpTransport transport = new NetHttpTransport();
		HttpRequestFactory factory = transport.createRequestFactory();
		return factory;
	}

	// //配置拦截器
	@Bean
	public WebMvcConfigurerAdapter webMvcConfigurerAdapter() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addInterceptors(InterceptorRegistry registry) {
				registry.addInterceptor(interceptor);
			}
		};
	}

	// @Bean
	// public WebMvcConfigurerAdapter forwardToIndex() {
	// return new WebMvcConfigurerAdapter() {
	// @Override
	// public void addViewControllers(ViewControllerRegistry registry) {
	// // forward requests to /admin and /user to their index.html
	// registry.addViewController("/").setViewName(
	// "forward:/admin/index.html");
	// registry.addViewController("/user").setViewName(
	// "forward:/user/index.html");
	// }
	// };
	// }

	@Bean
	public EmbeddedServletContainerCustomizer containerCustomizer() {
		return new MyCustomizer();
	}

}
