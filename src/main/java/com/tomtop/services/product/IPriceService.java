package com.tomtop.services.product;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.tomtop.valueobjects.product.price.Price;
import com.tomtop.valueobjects.product.price.PriceCalculationContext;
import com.tomtop.valueobjects.product.spec.IProductSpec;

public interface IPriceService {

	Price getPrice(String listingID,int storageId);

	Price getPrice(String listingID, String ccy,int storageId);

	Price getPrice(IProductSpec item,int storageId);

	Price getPrice(IProductSpec item, String ccy,int storageId);

	List<Price> getPrice(List<IProductSpec> items,int storageId);

	List<Price> getPrice(List<IProductSpec> items, Date priceAt,int storageId);

	List<Price> getPrice(List<IProductSpec> items, Date priceAt, String currency,int storageId);

	List<Price> getPrice(List<IProductSpec> items, PriceCalculationContext ctx);

	Map<IPriceProvider, List<Price>> getPriceByProvider(List<IProductSpec> items,int storageId);

}
