package com.tomtop.services.impl.product;

import java.util.List;

import org.apache.commons.chain.web.WebContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.tomtop.dto.product.ProductBase;
import com.tomtop.mappers.product.ProductBaseMapper;
import com.tomtop.services.impl.order.OrderService;
import com.tomtop.services.product.IProductService;
import com.tomtop.valueobjects.product.ProductLite;
import com.tomtop.valueobjects.product.Weight;

@Service
public class ProductService implements IProductService {

	@Autowired
	ProductBaseMapper productBaseMapper;
	
	private static final Logger Logger = LoggerFactory
			.getLogger(ProductService.class);
	
	@Override
	public ProductBase getBaseByListingIdAndLanguage(String listingId,
			Integer languageId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProductBase getBaseByListingId(String listingId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getListingIdByParentSkuAndWebsiteIdAndStatusAndIsMain(
			String parentsku, Integer isstatus, Integer websiteId,
			boolean ismain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ProductBase> getProductsWithSameParentSkuMatchingAttributes(
			String listingID, WebContext context) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> selectListingidBySearchNameAndSort(WebContext context,
			String searchname, String sort, Integer categoryId,
			List<String> pcListingIds) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ProductBase getProductByListingIdAndLanguageWithdoutDesc(
			String listingId, Integer languageId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProductDescByListingIdAndLanguagePart(String listingId,
			Integer languageId, int begin, int len) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ProductBase> getProductBasesByListingIds(List<String> listingids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ProductLite> getProductLiteByListingIDs(
			List<String> listingIDs, int websiteID, int languageID) {
		if (listingIDs != null && listingIDs.size() > 0) {
			List<ProductLite> plist = Lists.newArrayList();
			try {
				plist = productBaseMapper.getProductByListingIDs(listingIDs,
						websiteID, languageID);
			} catch (Exception ex) {
				Logger.error(ex.toString());
			}
			return plist;
		}
		return Lists.newArrayList();
	}

	@Override
	public List<ProductBase> getProductBaseBySkus(List<String> skus,
			Integer siteid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getCountBundleProduct(String main, String bundle) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Weight> getWeightList(List<String> listingIds) {
		// modify by lijun
		if (listingIds == null || listingIds.size() == 0) {
			throw new NullPointerException("listingIds is null");
		}
		return productBaseMapper.getWeightByListingIDs(listingIds);
	}
	
	
	/**
	 * 检查库存是否足够
	 * @param listingID
	 * @param qty
	 * @return
	 */
	public boolean checkInventory(String listingID, Integer qty) {
		int inv = productBaseMapper.getQtyByListingID(listingID);
		boolean res = false;
		if (null == qty && inv > 0) {
			res = true;
		} else if (null != qty && inv >= qty) {
			res = true;
		}
		return res;
	}
	
}
