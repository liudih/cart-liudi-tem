package com.tomtop.services.impl.product.price;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tomtop.services.ICurrencyService;
import com.tomtop.services.product.IPriceProvider;
import com.tomtop.services.product.IPriceService;
import com.tomtop.valueobjects.product.price.Price;
import com.tomtop.valueobjects.product.price.PriceCalculationContext;
import com.tomtop.valueobjects.product.spec.IProductSpec;
import com.tomtop.valueobjects.product.spec.ProductSpecBuilder;

@Service
public class PriceService implements IPriceService {

	private static final Logger Logger = LoggerFactory
			.getLogger(PriceService.class);

	@Autowired
	Set<IPriceProvider> priceProviders;

	@Autowired
	Set<IDiscountProvider> discountProviders;

	@Autowired
	ICurrencyService currency;

	@Autowired
	PriceCalculationContextFactory ctxFactory;

	@Override
	public Price getPrice(String listingID, int storageId) {
		return getPrice(ProductSpecBuilder.build(listingID).get(),
				ctxFactory.create(storageId));
	}

	@Override
	public Price getPrice(String listingID, String ccy, int storageId) {
		return getPrice(ProductSpecBuilder.build(listingID).get(),
				ctxFactory.create(ccy, storageId));
	}

	@Override
	public Price getPrice(IProductSpec item, int storageId) {
		return getPrice(item, ctxFactory.create(storageId));
	}

	@Override
	public Price getPrice(IProductSpec item, String ccy, int storageId) {
		return getPrice(item, ctxFactory.create(ccy, storageId));
	}

	public Price getPrice(IProductSpec item, PriceCalculationContext ctx) {
		List<Price> prices = getPrice(Lists.newArrayList(item), ctx);
		if (prices != null && prices.size() == 1) {
			return prices.get(0);
		}
		Logger.error("Price Calculation Error: Listing={}, Prices={}",
				item.getListingID(), prices);
		return null;
	}

	@Override
	public List<Price> getPrice(List<IProductSpec> items, int storageId) {
		return getPrice(items, ctxFactory.create(storageId));
	}

	@Override
	public List<Price> getPrice(List<IProductSpec> items, Date priceAt,
			int storageId) {
		PriceCalculationContext ctx = ctxFactory.create(storageId);
		if (priceAt != null) {
			ctx.setPriceAt(priceAt);
		}
		return getPrice(items, ctx);
	}

	@Override
	public List<Price> getPrice(List<IProductSpec> items, Date priceAt,
			String currency, int storageId) {
		PriceCalculationContext ctx = ctxFactory.create(currency, storageId);
		if (priceAt != null) {
			ctx.setPriceAt(priceAt);
		}
		return getPrice(items, ctx);
	}

	@Override
	public List<Price> getPrice(List<IProductSpec> items,
			final PriceCalculationContext ctx) {
		Collection<List<Price>> values = getPriceByProvider(items, ctx)
				.values();
		List<Price> flatten = Lists.newLinkedList();
		for (List<Price> p : values) {
			flatten.addAll(p);
		}
		return flatten;
	}

	@Override
	public Map<IPriceProvider, List<Price>> getPriceByProvider(
			final List<IProductSpec> items, int storageId) {
		return getPriceByProvider(items, ctxFactory.create(storageId));
	}

	public Map<IPriceProvider, List<Price>> getPriceByProvider(
			final List<IProductSpec> items, final PriceCalculationContext ctx) {
		Map<IPriceProvider, List<Price>> priceMap = Maps.asMap(
				priceProviders,
				pp -> {
					List<IProductSpec> filtered = Lists.newArrayList(Iterables
							.filter(items, i -> pp.match(i)));
					List<Price> prices = pp.getPrice(filtered, ctx);
					return prices;
				});

		List<IDiscountProvider> dps = Lists.newArrayList(discountProviders);
		Collections.sort(dps, (Comparator<IDiscountProvider>) (a, b) -> (a
				.getPriority() - b.getPriority()));
		Map<IPriceProvider, List<Price>> discounted = Maps.transformValues(
				priceMap, (List<Price> lp) -> {
					List<Price> current = lp;
					for (IDiscountProvider dp : dps) {
						current = dp.decorate(current, ctx);
					}
					return current;
				});
		return discounted;
	}
}
